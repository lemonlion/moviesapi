﻿using System;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using MoviesApiTest.Domain.Models;

namespace MoviesApiTest.Data.EntityFramework
{
    public interface IMoviesApiTestDbContext : IDisposable
    {
        DbSet<Movie> Movies { get; set; }
        DbSet<MovieUserRating> MovieUserRatings { get; set; }
        DbSet<User> Users { get; set; }

        Task SaveChangesAsync();
    }
}