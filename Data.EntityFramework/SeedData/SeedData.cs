﻿using System;
using System.Collections.Generic;
using System.Text;
using MoviesApiTest.Domain.Enums;
using MoviesApiTest.Domain.Models;

namespace MoviesApiTest.Data.EntityFramework.SeedData
{
    public static class SeedData
    {
        public static IEnumerable<Movie> Movies => new List<Movie>
        {
            new Movie { Id = 1, Title = "Jurassic Park", Genre = Genre.Action, YearOfRelease = 1993, RunningTime = 90 },
            new Movie { Id = 2, Title = "Minority Report", Genre = Genre.ScienceFiction, YearOfRelease = 2002, RunningTime = 91 },
            new Movie { Id = 3, Title = "Comedy Movie", Genre = Genre.Comedy, YearOfRelease = 1994, RunningTime = 92 },
            new Movie { Id = 4, Title = "Crime Movie", Genre = Genre.Crime, YearOfRelease = 1995, RunningTime = 93 },
            new Movie { Id = 5, Title = "Drama Movie", Genre = Genre.Drama, YearOfRelease = 1996, RunningTime = 94 },
            new Movie { Id = 6, Title = "Fantasy Movie", Genre = Genre.Fantasy, YearOfRelease = 1997, RunningTime = 95 },
            new Movie { Id = 7, Title = "Historical Movie", Genre = Genre.Historical, YearOfRelease = 1998, RunningTime = 96 },
            new Movie { Id = 8, Title = "Horror Movie", Genre = Genre.Horror, YearOfRelease = 1999, RunningTime = 97 },
            new Movie { Id = 9, Title = "The Good the Bad and the Ugly", Genre = Genre.Western, YearOfRelease = 1990, RunningTime = 98 }
        };

        public static IEnumerable<MovieUserRating> MovieUserRatings => new List<MovieUserRating>
        {
            new MovieUserRating { MovieId = 1, UserId = 1, Rating = 5 },
            new MovieUserRating { MovieId = 1, UserId = 2, Rating = 4 },
            new MovieUserRating { MovieId = 1, UserId = 3, Rating = 3 },
            new MovieUserRating { MovieId = 2, UserId = 1, Rating = 5 },
            new MovieUserRating { MovieId = 2, UserId = 2, Rating = 5 },
            new MovieUserRating { MovieId = 2, UserId = 3, Rating = 5 },
            new MovieUserRating { MovieId = 3, UserId = 1, Rating = 4 },
            new MovieUserRating { MovieId = 3, UserId = 2, Rating = 4 },
            new MovieUserRating { MovieId = 3, UserId = 3, Rating = 4 },
            new MovieUserRating { MovieId = 4, UserId = 1, Rating = 5 },
            new MovieUserRating { MovieId = 4, UserId = 2, Rating = 4 },
            new MovieUserRating { MovieId = 4, UserId = 3, Rating = 1 },
            new MovieUserRating { MovieId = 5, UserId = 1, Rating = 1 },
            new MovieUserRating { MovieId = 5, UserId = 2, Rating = 3 },
            new MovieUserRating { MovieId = 5, UserId = 3, Rating = 4 },
            new MovieUserRating { MovieId = 6, UserId = 1, Rating = 1 },
            new MovieUserRating { MovieId = 6, UserId = 2, Rating = 1 },
            new MovieUserRating { MovieId = 6, UserId = 3, Rating = 5 },
            new MovieUserRating { MovieId = 7, UserId = 1, Rating = 2 },
            new MovieUserRating { MovieId = 7, UserId = 2, Rating = 1 },
            new MovieUserRating { MovieId = 7, UserId = 3, Rating = 3 },
            new MovieUserRating { MovieId = 8, UserId = 1, Rating = 1 },
            new MovieUserRating { MovieId = 8, UserId = 2, Rating = 2 },
            new MovieUserRating { MovieId = 8, UserId = 3, Rating = 4 },
            new MovieUserRating { MovieId = 9, UserId = 1, Rating = 1 },
            new MovieUserRating { MovieId = 9, UserId = 2, Rating = 2 },
            new MovieUserRating { MovieId = 9, UserId = 3, Rating = 3 }
        };

        public static IEnumerable<User> Users => new List<User>
        {
            new User { Id = 1, Name = "Larry" },
            new User { Id = 2, Name = "Curly" },
            new User { Id = 3, Name = "Moe" },
            new User { Id = 4, Name = "Johnny NoRating" }
        };
    }
}
