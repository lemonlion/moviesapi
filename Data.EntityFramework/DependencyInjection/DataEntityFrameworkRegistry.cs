﻿using StructureMap;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MoviesApiTest.Domain.DependencyInjection;

namespace MoviesApiTest.Data.EntityFramework.DependencyInjection
{
    public class DataEntityFrameworkRegistry : Registry
    {
        public DataEntityFrameworkRegistry()
        {
            IncludeRegistry<DomainRegistry>();

            Scan(x =>
            {
                x.AssemblyContainingType<DataEntityFrameworkRegistry>();
                x.WithDefaultConventions();
            });
        }
    }
}
