﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace MoviesApiTest.Data.EntityFramework.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Movies",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Title = table.Column<string>(nullable: true),
                    YearOfRelease = table.Column<int>(nullable: false),
                    Genre = table.Column<int>(nullable: false),
                    RunningTime = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Movies", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "MovieUserRatings",
                columns: table => new
                {
                    UserId = table.Column<int>(nullable: false),
                    MovieId = table.Column<int>(nullable: false),
                    Rating = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MovieUserRatings", x => new { x.MovieId, x.UserId });
                    table.ForeignKey(
                        name: "FK_MovieUserRatings_Movies_MovieId",
                        column: x => x.MovieId,
                        principalTable: "Movies",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_MovieUserRatings_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.InsertData(
                table: "Movies",
                columns: new[] { "Id", "Genre", "RunningTime", "Title", "YearOfRelease" },
                values: new object[,]
                {
                    { 1, 4, 90, "Jurassic Park", 1993 },
                    { 2, 8, 91, "Minority Report", 2002 },
                    { 3, 1, 92, "Comedy Movie", 1994 },
                    { 4, 5, 93, "Crime Movie", 1995 },
                    { 5, 0, 94, "Drama Movie", 1996 },
                    { 6, 10, 95, "Fantasy Movie", 1997 },
                    { 7, 9, 96, "Historical Movie", 1998 },
                    { 8, 2, 97, "Horror Movie", 1999 },
                    { 9, 7, 98, "The Good the Bad and the Ugly", 1990 }
                });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "Name" },
                values: new object[,]
                {
                    { 1, "Larry" },
                    { 2, "Curly" },
                    { 3, "Moe" },
                    { 4, "Johnny NoRating" }
                });

            migrationBuilder.InsertData(
                table: "MovieUserRatings",
                columns: new[] { "MovieId", "UserId", "Rating" },
                values: new object[,]
                {
                    { 1, 1, 5 },
                    { 7, 3, 3 },
                    { 6, 3, 5 },
                    { 5, 3, 4 },
                    { 4, 3, 1 },
                    { 3, 3, 4 },
                    { 2, 3, 5 },
                    { 1, 3, 3 },
                    { 9, 2, 2 },
                    { 8, 2, 2 },
                    { 7, 2, 1 },
                    { 6, 2, 1 },
                    { 8, 3, 4 },
                    { 5, 2, 3 },
                    { 3, 2, 4 },
                    { 2, 2, 5 },
                    { 1, 2, 4 },
                    { 9, 1, 1 },
                    { 8, 1, 1 },
                    { 7, 1, 2 },
                    { 6, 1, 1 },
                    { 5, 1, 1 },
                    { 4, 1, 5 },
                    { 3, 1, 4 },
                    { 2, 1, 5 },
                    { 4, 2, 4 },
                    { 9, 3, 3 }
                });

            migrationBuilder.CreateIndex(
                name: "IX_MovieUserRatings_UserId",
                table: "MovieUserRatings",
                column: "UserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "MovieUserRatings");

            migrationBuilder.DropTable(
                name: "Movies");

            migrationBuilder.DropTable(
                name: "Users");
        }
    }
}
