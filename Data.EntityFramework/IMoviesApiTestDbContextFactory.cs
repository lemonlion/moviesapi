﻿namespace MoviesApiTest.Data.EntityFramework
{
    public interface IMoviesApiTestDbContextFactory
    {
        MoviesApiTestDbContext CreateDbContext(string[] args);
        IMoviesApiTestDbContext CreateIDbContext(string[] args = null);
    }
}