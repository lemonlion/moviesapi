﻿using Microsoft.EntityFrameworkCore;
using MoviesApiTest.Domain.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using MoviesApiTest.Domain.Enums;
using MoviesApiTest.Data.EntityFramework.SeedData;

namespace MoviesApiTest.Data.EntityFramework
{
    public class MoviesApiTestDbContext : DbContext, IMoviesApiTestDbContext
    {
        public MoviesApiTestDbContext(DbContextOptions<MoviesApiTestDbContext> options) : base(options)
        { }

        public DbSet<User> Users { get; set; }
        public DbSet<Movie> Movies { get; set; }
        public DbSet<MovieUserRating> MovieUserRatings { get; set; }

        public Task SaveChangesAsync() => base.SaveChangesAsync();

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<MovieUserRating>()
                .HasKey(x => new { x.MovieId, x.UserId });
            modelBuilder.Entity<MovieUserRating>()
                .HasOne(bc => bc.Movie)
                .WithMany(b => b.UserRatings)
                .HasForeignKey(bc => bc.MovieId)
                .OnDelete(DeleteBehavior.Restrict);
            modelBuilder.Entity<MovieUserRating>()
                .HasOne(bc => bc.User)
                .WithMany(c => c.MovieRatings)
                .HasForeignKey(bc => bc.UserId)
                .OnDelete(DeleteBehavior.Restrict);

            Seed(modelBuilder);
        }

        private void Seed(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Movie>().HasData(SeedData.SeedData.Movies);

            modelBuilder.Entity<User>().HasData(SeedData.SeedData.Users);

            modelBuilder.Entity<MovieUserRating>().HasData(SeedData.SeedData.MovieUserRatings);
        }
    }
}
