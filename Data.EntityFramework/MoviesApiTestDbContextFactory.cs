﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Extensions.Configuration;

namespace MoviesApiTest.Data.EntityFramework
{
    public class MoviesApiTestDbContextFactory : IDesignTimeDbContextFactory<MoviesApiTestDbContext>, IMoviesApiTestDbContextFactory
    {
        public MoviesApiTestDbContext CreateDbContext(string[] args)
        {
            // Build config
            IConfiguration config = new ConfigurationBuilder()
                .SetBasePath(AppContext.BaseDirectory)
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile($"appsettings.{Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") ?? "Production"}.json", optional: true)
                .AddEnvironmentVariables()
                .Build();

            // Debugger.Launch();
            var optionsBuilder = new DbContextOptionsBuilder<MoviesApiTestDbContext>();
            optionsBuilder.UseSqlServer(config.GetConnectionString("MoviesApiDatabase"));

            return new MoviesApiTestDbContext(optionsBuilder.Options);
        }

        public IMoviesApiTestDbContext CreateIDbContext(string[] args = null) => CreateDbContext(args);
    }
}
