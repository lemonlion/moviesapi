﻿using StructureMap;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FluentValidation;
using MoviesApiTest.Api.Validation;
using MoviesApiTest.Domain.QueryModels;
using MoviesApiTest.Domain.WriteModels;
using MoviesApiTest.Services.DependencyInjection;

namespace MoviesApiTest.Api.DependencyInjection
{
    public class ApiRegistry : Registry
    {
        public ApiRegistry()
        {
            IncludeRegistry<ServicesRegistry>();

            Scan(x =>
            {
                //x.TheCallingAssembly();
                x.AssemblyContainingType<ApiRegistry>();
                x.WithDefaultConventions();
            });
            
            For<IValidator<MovieQueryModel>>().Use<MovieQueryModelValidator>();
            For<IValidator<MovieRatingWriteModel>>().Use<MovieRatingWriteModelValidator>();
        }
    }
}
