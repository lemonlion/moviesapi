﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FluentValidation;
using MoviesApiTest.Domain.QueryModels;
using MoviesApiTest.Domain.WriteModels;

namespace MoviesApiTest.Api.Validation
{
    public class MovieRatingWriteModelValidator : AbstractValidator<MovieRatingWriteModel>
    {
        public MovieRatingWriteModelValidator()
        {
            RuleFor(x => x.Rating).GreaterThanOrEqualTo(1)
                                  .LessThanOrEqualTo(5);
        }
    }
}
