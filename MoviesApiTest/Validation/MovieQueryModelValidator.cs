﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FluentValidation;
using MoviesApiTest.Domain.QueryModels;

namespace MoviesApiTest.Api.Validation
{
    public class MovieQueryModelValidator : AbstractValidator<MovieQueryModel>
    {
        public MovieQueryModelValidator()
        {
            When(x => x.Genre == null && x.Title == null && x.YearOfRelease == null, () => {
                RuleFor(x => x.Title).NotNull().WithMessage("You must specify a filter for genre, title or yearOfRelease");
            });
        }
    }
}
