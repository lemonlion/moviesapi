﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore.Internal;
using MoviesApiTest.Domain.Models;
using MoviesApiTest.Domain.QueryModels;
using MoviesApiTest.Domain.WriteModels;
using MoviesApiTest.Services;

namespace MoviesApiTest.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MoviesController : ControllerBase
    {
        private readonly IMovieService _movieService;

        public MoviesController(IMovieService movieService)
        {
            _movieService = movieService;
        }

        [HttpGet("")]
        public async Task<ActionResult<IEnumerable<MovieReadModel>>> Get([FromQuery]MovieQueryModel model)
        {
            var movies = await _movieService.GetMoviesFiltered(model.Title, model.YearOfRelease, model.Genre);
            return OkOrNotFound(movies);
        }

        [HttpGet("top-five/average-user-rated")]
        public async Task<ActionResult<IEnumerable<MovieReadModel>>> GetTop5AverageUserRating()
        {
            var movies = await _movieService.GetTop5MoviesBasedOnAverageRatings();
            return OkOrNotFound(movies);
        }

        [HttpGet("top-five/user-rated/{userId}")]
        public async Task<ActionResult<IEnumerable<MovieReadModel>>> GetTop5UserRating(int userId)
        {
            var movies = await _movieService.GetTop5MoviesBasedOnHighestRatingsForUser(userId);
            return OkOrNotFound(movies);
        }

        private ActionResult<IEnumerable<MovieReadModel>> OkOrNotFound(IEnumerable<MovieReadModel> movies)
        {
            if (movies?.Any() != true)
                return NotFound();

            return Ok(movies);
        }
    }
}
