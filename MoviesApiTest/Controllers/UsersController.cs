﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MoviesApiTest.Domain.WriteModels;
using MoviesApiTest.Services;

namespace MoviesApiTest.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly IMovieRatingService _movieRatingService;

        public UsersController(IMovieRatingService movieRatingService)
        {
            _movieRatingService = movieRatingService;
        }

        [HttpPut("{id}/movie-rating")]
        public async Task Put(int id, [FromBody] MovieRatingWriteModel model)
        {
            model.UserId = id;

            await _movieRatingService.AddOrUpdateMovieRating(model.UserId, model.MovieId, model.Rating);
        }
    }
}
