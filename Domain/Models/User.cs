﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MoviesApiTest.Domain.Models
{
    public class User
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public virtual ICollection<MovieUserRating> MovieRatings { get; set; }
    }
}
