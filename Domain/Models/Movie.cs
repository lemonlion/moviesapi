﻿using MoviesApiTest.Domain.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace MoviesApiTest.Domain.Models
{
    public class Movie
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public int YearOfRelease { get; set; }
        public Genre Genre { get; set; }

        /// <summary>
        /// Running Time in minutes
        /// </summary>
        public int RunningTime { get; set; }

        public virtual ICollection<MovieUserRating> UserRatings { get; set; }
    }
}
