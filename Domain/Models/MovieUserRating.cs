﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MoviesApiTest.Domain.Models
{
    public class MovieUserRating
    {
        public int UserId { get; set; }
        public int MovieId { get; set; }
        public int Rating { get; set; }

        public virtual User User { get; set; }
        public virtual Movie Movie { get; set; }
    }
}
