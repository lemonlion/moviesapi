﻿using MoviesApiTest.Domain.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace MoviesApiTest.Domain.Models
{
    public class MovieReadModel
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public int YearOfRelease { get; set; }
        public Genre Genre { get; set; }
        public decimal AverageRating { get; set; }
        
        /// <summary>
        /// Running time in minutes
        /// </summary>
        public int RunningTime { get; set; }
    }
}
