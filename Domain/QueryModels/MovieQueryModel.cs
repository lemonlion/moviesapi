﻿using MoviesApiTest.Domain.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace MoviesApiTest.Domain.QueryModels
{
    public class MovieQueryModel
    {
        public string Title { get; set; }
        public int? YearOfRelease { get; set; }
        public Genre? Genre { get; set; }
    }
}
