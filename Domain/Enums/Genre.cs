﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MoviesApiTest.Domain.Enums
{
    public enum Genre
    {
        Drama,
        Comedy,
        Horror,
        Thriller,
        Action,
        Crime,
        War,
        Western,
        ScienceFiction,
        Historical,
        Fantasy
    }
}
