﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MoviesApiTest.Domain.WriteModels
{
    public class MovieRatingWriteModel
    {
        public int UserId { get; set; }
        public int MovieId { get; set; }
        public int Rating { get; set; }
    }
}
