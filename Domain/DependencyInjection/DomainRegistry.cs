﻿using StructureMap;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MoviesApiTest.Domain.DependencyInjection
{
    public class DomainRegistry : Registry
    {
        public DomainRegistry()
        {
            Scan(x =>
            {
                x.AssemblyContainingType<DomainRegistry>();
                x.WithDefaultConventions();
            });
        }
    }
}
