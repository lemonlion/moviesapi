# Movies API #

This is a technical exercise I did in a few hours for a company called FreeWheel.

The Specifications document is in the root folder, it's called "Specifications.pdf"

Their spec that wanted me to put the Movie controller's three GET calls as three separate endpoints, while keeping it RESTful rather than RPC, which I found a little tricky, but did the best I could.  Always open to more suggestions there though.