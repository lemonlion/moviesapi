﻿using MoreLinq;
using MoviesApiTest.Data.EntityFramework;
using MoviesApiTest.Domain.Enums;
using MoviesApiTest.Domain.Models;
using MoviesApiTest.Services.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace MoviesApiTest.Services
{
    public class MovieRatingService : IMovieRatingService
    {
        private readonly IMoviesApiTestDbContextFactory _dbContextFactory;

        public MovieRatingService(IMoviesApiTestDbContextFactory dbContextFactory)
        {
            _dbContextFactory = dbContextFactory;
        }

        public async Task AddOrUpdateMovieRating(int userId, int movieId, int rating)
        {
            using (var dbContext = _dbContextFactory.CreateIDbContext())
            {
                var user = await dbContext.Users.Include(x => x.MovieRatings).SingleOrDefaultAsync(x => x.Id == userId);
                if (user == null)
                    throw new Exception($"User of id {userId} doesn't exist");

                var movie = dbContext.Movies.SingleOrDefault(x => x.Id == movieId);
                if(movie == null)
                    throw new Exception($"Movie of id {movieId} doesn't exist");

                var movieRating = user.MovieRatings.SingleOrDefault(x => x.MovieId == movieId);
                if (movieRating == null)
                    user.MovieRatings.Add(new MovieUserRating {User = user, Movie = movie, Rating = rating});
                else
                    movieRating.Rating = rating;

                await dbContext.SaveChangesAsync();
            }
        }
    }
}
