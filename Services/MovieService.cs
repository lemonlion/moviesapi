﻿using MoreLinq;
using MoviesApiTest.Data.EntityFramework;
using MoviesApiTest.Domain.Enums;
using MoviesApiTest.Domain.Models;
using MoviesApiTest.Services.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace MoviesApiTest.Services
{
    public class MovieService : IMovieService
    {
        private readonly IMoviesApiTestDbContextFactory _dbContextFactory;

        public MovieService(IMoviesApiTestDbContextFactory dbContextFactory)
        {
            _dbContextFactory = dbContextFactory;
        }
        
        public async Task<IEnumerable<MovieReadModel>> GetMoviesFiltered(string title = null, int? yearOfRelease = null, Genre? genre = null)
        {
            using (var dbContext = _dbContextFactory.CreateIDbContext())
            {
                var query = dbContext.Movies.AsQueryable();

                if (!string.IsNullOrWhiteSpace(title))
                    query = query.Where(x => x.Title.Contains(title));

                if (yearOfRelease != null)
                    query = query.Where(x => x.YearOfRelease == yearOfRelease);

                if (genre != null)
                    query = query.Where(x => x.Genre == genre);

                var results = await query.Select(x => new MovieReadModel
                {
                    Genre = x.Genre,
                    Id = x.Id,
                    Title = x.Title,
                    YearOfRelease = x.YearOfRelease,
                    RunningTime = x.RunningTime,
                    AverageRating = (decimal)x.UserRatings.Average(y => y.Rating)
                }).ToListAsync();

                foreach (var movie in results)
                    movie.AverageRating = movie.AverageRating.RoundToNearestPoint5();

                return results;
            }
        }

        public async Task<IEnumerable<MovieReadModel>> GetTop5MoviesBasedOnAverageRatings()
        {
            using (var dbContext = _dbContextFactory.CreateIDbContext())
            {
                var readModels = await dbContext.Movies.Select(x => new MovieReadModel
                {
                    Genre = x.Genre,
                    Id = x.Id,
                    Title = x.Title,
                    YearOfRelease = x.YearOfRelease,
                    RunningTime = x.RunningTime,
                    AverageRating = (decimal)x.UserRatings.Average(y => y.Rating)
                }).OrderByDescending(x => x.AverageRating).ThenBy(x => x.Title).Take(5).ToListAsync();

                foreach (var movie in readModels)
                    movie.AverageRating = movie.AverageRating.RoundToNearestPoint5();

                return readModels;
            }
        }

        public async Task<IEnumerable<MovieReadModel>> GetTop5MoviesBasedOnHighestRatingsForUser(int userId)
        {
            using (var dbContext = _dbContextFactory.CreateIDbContext())
            {
                var result = await dbContext.Users.Where(x => x.Id == userId)
                                            .SelectMany(x => x.MovieRatings)
                                            .OrderByDescending(x => x.Rating)
                                            .Take(5)
                                            .Select(x => new MovieReadModel
                                            {
                                                Genre = x.Movie.Genre,
                                                Id = x.Movie.Id,
                                                Title = x.Movie.Title,
                                                YearOfRelease = x.Movie.YearOfRelease,
                                                RunningTime = x.Movie.RunningTime,
                                                AverageRating = (decimal)x.Movie.UserRatings.Average(y => y.Rating)
                                            }).ToListAsync();

                foreach (var movie in result)
                    movie.AverageRating = movie.AverageRating.RoundToNearestPoint5();

                return result;
            }
        }
    }
}
