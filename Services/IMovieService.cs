﻿using System.Collections.Generic;
using System.Threading.Tasks;
using MoviesApiTest.Domain.Enums;
using MoviesApiTest.Domain.Models;

namespace MoviesApiTest.Services
{
    public interface IMovieService
    {
        Task<IEnumerable<MovieReadModel>> GetMoviesFiltered(string title = null, int? yearOfRelease = null, Genre? genre = null);
        Task<IEnumerable<MovieReadModel>> GetTop5MoviesBasedOnAverageRatings();
        Task<IEnumerable<MovieReadModel>> GetTop5MoviesBasedOnHighestRatingsForUser(int userId);
    }
}