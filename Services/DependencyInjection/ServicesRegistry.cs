﻿using StructureMap;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MoviesApiTest.Data.EntityFramework.DependencyInjection;
using MoviesApiTest.Domain.DependencyInjection;

namespace MoviesApiTest.Services.DependencyInjection
{
    public class ServicesRegistry : Registry
    {
        public ServicesRegistry()
        {
            IncludeRegistry<DomainRegistry>();
            IncludeRegistry<DataEntityFrameworkRegistry>();

            Scan(x =>
            {
                x.AssemblyContainingType<ServicesRegistry>();
                x.WithDefaultConventions();
            });
        }
    }
}
