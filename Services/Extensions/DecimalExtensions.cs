﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MoviesApiTest.Services.Extensions
{
    public static class DecimalExtensions
    {
        public static decimal RoundToNearestPoint5(this decimal number) => Math.Round(number * 2, MidpointRounding.AwayFromZero) / 2;
    }
}
