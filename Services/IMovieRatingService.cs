﻿using System.Threading.Tasks;

namespace MoviesApiTest.Services
{
    public interface IMovieRatingService
    {
        Task AddOrUpdateMovieRating(int userId, int movieId, int rating);
    }
}