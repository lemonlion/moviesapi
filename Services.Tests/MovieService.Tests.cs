using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Moq;
using MoviesApiTest.Data.EntityFramework;
using MoviesApiTest.Data.EntityFramework.SeedData;
using Xunit;
using Shouldly;
using MoviesApiTest.Services;

namespace MoviesApiTest.Services.Tests
{
    public class MovieServiceTests
    {
        private MovieService _serviceUnderTest;
        private Mock<IMoviesApiTestDbContextFactory> _dbContextFactoryMock = new Mock<IMoviesApiTestDbContextFactory>();
        private MoviesApiTestDbContext _dbContext;

        public MovieServiceTests()
        {
            // Arrange
            var dbContextOptionsBuilder = new DbContextOptionsBuilder<MoviesApiTestDbContext>();
            dbContextOptionsBuilder.UseInMemoryDatabase("MoviesApiTestDb");
            _dbContext = new MoviesApiTestDbContext(dbContextOptionsBuilder.Options);
            _dbContext.AddRange(SeedData.Movies);
            _dbContext.AddRange(SeedData.Users);
            _dbContext.AddRange(SeedData.MovieUserRatings);
            _dbContext.SaveChangesAsync();

            _dbContextFactoryMock.Setup(x => x.CreateIDbContext(null)).Returns(_dbContext);
            _serviceUnderTest = new MovieService(_dbContextFactoryMock.Object);
        }

        [Fact]
        public async Task Calling_GetTop5MoviesBasedOnAverageRatings_Returns_5_Results()
        {
            // Act
            var result = await _serviceUnderTest.GetTop5MoviesBasedOnAverageRatings();

            // Assert
            result.Count().ShouldBe(5);
        }

        [Fact]
        public async Task Calling_GetTop5MoviesBasedOnAverageRatings_Returns_Results_With_Average_Ratings_To_Nearest_Point_5()
        {
            // Act
            var result = await _serviceUnderTest.GetTop5MoviesBasedOnAverageRatings();

            // Assert
            result.Count().ShouldBe(5);
        }

        [Fact]
        public async Task Calling_GetTop5MoviesBasedOnAverageRatings_Returns_Correctly_Ordered_Results()
        {
            // Act
            var result = await _serviceUnderTest.GetTop5MoviesBasedOnAverageRatings();

            // Assert
            result.ElementAt(0).AverageRating.ShouldBeGreaterThanOrEqualTo(result.ElementAt(1).AverageRating);
            result.ElementAt(1).AverageRating.ShouldBeGreaterThanOrEqualTo(result.ElementAt(2).AverageRating);
            result.ElementAt(2).AverageRating.ShouldBeGreaterThanOrEqualTo(result.ElementAt(3).AverageRating);
            result.ElementAt(3).AverageRating.ShouldBeGreaterThanOrEqualTo(result.ElementAt(4).AverageRating);
        }

        [Fact]
        public async Task Calling_GetTop5MoviesBasedOnAverageRatings_Returns_Correct_5_Results()
        {
            // Act
            var result = await _serviceUnderTest.GetTop5MoviesBasedOnAverageRatings();

            // Assert
            result.ElementAt(0).Title.ShouldBe("Minority Report");
            result.ElementAt(1).Title.ShouldBe("Comedy Movie");
            result.ElementAt(2).Title.ShouldBe("Jurassic Park");
            result.ElementAt(3).Title.ShouldBe("Crime Movie");
            result.ElementAt(4).Title.ShouldBe("Drama Movie");
        }


        [Fact]
        public async Task Calling_GetTop5MoviesBasedOnHighestRatingsForUser_Returns_5_Results()
        {
            // Act
            var result = await _serviceUnderTest.GetTop5MoviesBasedOnHighestRatingsForUser(1);

            // Assert
            result.Count().ShouldBe(5);
        }

        [Fact]
        public async Task Calling_GetTop5MoviesBasedOnHighestRatingsForUser_Returns_Correct_5_Results()
        {
            // Act
            var result = await _serviceUnderTest.GetTop5MoviesBasedOnHighestRatingsForUser(1);

            // Assert
            result.ElementAt(0).Title.ShouldBe("Jurassic Park");
            result.ElementAt(1).Title.ShouldBe("Minority Report");
            result.ElementAt(2).Title.ShouldBe("Crime Movie");
            result.ElementAt(3).Title.ShouldBe("Comedy Movie");
            result.ElementAt(4).Title.ShouldBe("Historical Movie");
        }


        [Fact]
        public async Task Calling_GetMoviesFiltered_With_Title_Park_Returns_Jurassic_Park()
        {
            // Act
            var result = await _serviceUnderTest.GetMoviesFiltered(title: "Park");

            // Assert
            result.ShouldContain(x => x.Title == "Jurassic Park");
        }

        [Fact]
        public async Task Calling_GetMoviesFiltered_With_Title_Movie_Returns_Only_Movies_With_Movie_In_Title()
        {
            // Act
            var result = await _serviceUnderTest.GetMoviesFiltered(title: "Movie");

            // Assert
            result.All(x => x.Title.Contains("Movie")).ShouldBeTrue();
        }
    }
}
