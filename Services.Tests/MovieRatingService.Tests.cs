using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Moq;
using MoviesApiTest.Data.EntityFramework;
using MoviesApiTest.Data.EntityFramework.SeedData;
using Xunit;
using Shouldly;
using MoviesApiTest.Services;

namespace MoviesApiTest.Services.Tests
{
    public class MovieRatingServiceTests
    {
        private MovieRatingService _serviceUnderTest;
        private Mock<IMoviesApiTestDbContextFactory> _dbContextFactoryMock = new Mock<IMoviesApiTestDbContextFactory>();
        private MoviesApiTestDbContext _dbContext;
        private string _uniquePerInstanceDbName = $"MoviesApiTestDb-{Guid.NewGuid()}";

        public MovieRatingServiceTests()
        {
            // Arrange
            _dbContext = GetDbContext();
            _dbContext.AddRange(SeedData.Movies);
            _dbContext.AddRange(SeedData.Users);
            _dbContext.AddRange(SeedData.MovieUserRatings);
            _dbContext.SaveChangesAsync();

            _dbContextFactoryMock.Setup(x => x.CreateIDbContext(null)).Returns(_dbContext);
            _serviceUnderTest = new MovieRatingService(_dbContextFactoryMock.Object);
        }

        [Fact]
        public async Task Calling_AddOrUpdateMovieRating_With_Rating_For_Movie_Already_Rated_By_User_Updates_Movie_Rating()
        {
            // Act
            await _serviceUnderTest.AddOrUpdateMovieRating(1, 1, 2);

            // Assert
            GetDbContext().MovieUserRatings.Single(x => x.UserId == 1 && x.MovieId == 1).Rating.ShouldBe(2);
        }

        [Fact]
        public async Task Calling_AddOrUpdateMovieRating_With_Rating_For_Movie_Not_Rated_By_User_Adds_Movie_Rating()
        {
            // Act
            await _serviceUnderTest.AddOrUpdateMovieRating(4, 1, 2);

            // Assert
            GetDbContext().MovieUserRatings.Single(x => x.UserId == 4 && x.MovieId == 1).Rating.ShouldBe(2);
        }

        private MoviesApiTestDbContext GetDbContext()
        {
            var dbContextOptionsBuilder = new DbContextOptionsBuilder<MoviesApiTestDbContext>();
            dbContextOptionsBuilder.UseInMemoryDatabase(_uniquePerInstanceDbName);
            return new MoviesApiTestDbContext(dbContextOptionsBuilder.Options);
        }
    }
}
