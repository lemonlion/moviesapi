using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Moq;
using MoviesApiTest.Data.EntityFramework;
using MoviesApiTest.Data.EntityFramework.SeedData;
using Xunit;
using Shouldly;
using MoviesApiTest.Services;
using MoviesApiTest.Services.Extensions;

namespace MoviesApiTest.Services.Tests
{
    public class DecimalExtensionsTests
    {
        [Fact]
        public void RoundToNearestPoint5_Should_Round_Down_To_Nearest_Point_5() => 1.7m.RoundToNearestPoint5().ShouldBe(1.5m);

        [Fact]
        public void RoundToNearestPoint5_Should_Round_Up_To_Nearest_Point_5() => 1.3m.RoundToNearestPoint5().ShouldBe(1.5m);

    }
}
